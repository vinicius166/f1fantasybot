# F1FantasyBot
Discord Bot for a Formula 1 fantasy game.

**Running initial database state with liquibase**  
Execute the following maven runnable in eclipse:    
`liquibase:update -f pom.xml -Dliquibase.propertyFile=src/main/resources/liquibase.properties`

# Important - Setting up the environment
To run the bot you will need to set the bot's discord token into an environment property first.  

**Linux**  
To set an environment variable on Linux, enter the following command at a shell prompt, according to which shell you are using:  
**csh/tcsh:** `setenv botToken value`  
**bash/ksh:** `export botToken=value`  

**Windows**  
To set an environment variable on Windows, enter the following command at a shell prompt:  
`setx botToken "value" /M`
