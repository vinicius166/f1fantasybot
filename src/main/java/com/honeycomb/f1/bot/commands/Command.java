package com.honeycomb.f1.bot.commands;

public interface Command {
	
	public void execute();
	public boolean validate();
}
