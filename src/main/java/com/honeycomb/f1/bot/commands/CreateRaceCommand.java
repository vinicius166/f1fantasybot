package com.honeycomb.f1.bot.commands;

import java.awt.Color;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Logger;

import org.bson.Document;

import com.honeycomb.f1.bot.data.RaceDataSingleton;
import com.honeycomb.f1.bot.domain.Domains;
import com.honeycomb.f1.bot.domain.Emotes;
import com.honeycomb.f1.bot.model.Championship;
import com.honeycomb.f1.bot.model.Circuit;
import com.honeycomb.f1.bot.model.Race;
import com.honeycomb.f1.bot.repository.ChampionshipRepository;
import com.honeycomb.f1.bot.repository.CircuitRepository;
import com.honeycomb.f1.bot.repository.RaceRepository;
import com.honeycomb.f1.bot.service.MessageService;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.TextChannel;

public class CreateRaceCommand implements Command {
	
	Logger LOG = Logger.getLogger(this.getClass().getName());

	private Message message;
	private TextChannel textChannel;
	private JDA discordService;
	private ChampionshipRepository championshipRepository;
	private RaceRepository raceRepository;
	private Championship championship;
	
	public CreateRaceCommand(Message message, TextChannel textChannel, JDA jda) {
		this.message = message;
		this.textChannel = textChannel;
		this.discordService = jda;
		this.championshipRepository = new ChampionshipRepository();
		this.championship = new Championship();
		this.raceRepository = new RaceRepository();
	}
	
	@Override
	public void execute() {
		String[] args = message.getContentRaw().split(Domains.COMMAND_SPLIT_PATTERN.getName());
		MessageService messageService = MessageService.getInstance();
		
		if(!hasEnoughCommandParameters(args)) {
			LOG.severe("Not enough parameters on the !createRace command: " + args);
			textChannel.sendMessage(messageService.getMessage("race.missing.parameters")).queue();
			
		} else {
			
			if(validate()) {
				LOG.info("Validation sucessfull, creating race with Message ID: " + message.getId());
				Race race = createRace(args, message.getId());
				
				EmbedBuilder canvas = new EmbedBuilder();
				canvas.setTitle(args[1] + Domains.GRAND_PRIX.name);
		        canvas.setColor(Color.RED);
		        canvas.setDescription(getEmbedDescription(race));
		    	canvas.setThumbnail(race.getCircuit().getCountry().getFlagURL());
		    	canvas.setFooter(messageService.getMessage("canvas.footer.info"));
		    	canvas.addField(messageService.getMessage("canvas.qualify"), getCanvasDateTime(race.getQualifyDateTime()), true);
		    	canvas.addField(messageService.getMessage("canvas.race"), getCanvasDateTime(race.getRaceDateTime()), true);
				canvas.setImage(race.getCircuit().getCircuitURL());
				
		        MessageEmbed canvasMessage = canvas.build();
		        
		        Emote[] emoteArray = getEmotesByName();
		        textChannel.sendMessage(canvasMessage)
				 		   .queue(message -> {
				 			   addEmbedReactions(message, emoteArray);
				 			   race.setMessageId(message.getId());
				 			   raceRepository.createRace(race);
				 			   RaceDataSingleton.getInstance().setActiveRaceMessageId(message.getId());
				 		   });
			}
		}
	}

	private String getCanvasDateTime(LocalDateTime localDateTime) {
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
		return MessageService.getInstance().getMessage("canvas.link", dateFormat.format(localDateTime));
	}
	
	private String getEmbedDescription(Race race) {
		StringBuilder sb = new StringBuilder();
		sb.append(MessageService.getInstance().getMessage("canvas.location", race.getCircuit().getCountry().getName()))
		  .append(MessageService.getInstance().getMessage("canvas.laps", race.getLaps()));
		return sb.toString();
	}
	
	private Emote[] getEmotesByName() {
		LinkedList<Emote> emoteList = new LinkedList<Emote>();
		
		for (Emotes em : Emotes.values()) {
			Emote emote = discordService.getEmotesByName(em.getName(), true).stream()
																			.findFirst()
																			.get();
			emoteList.add(emote);
		}
		
		return emoteList.toArray(new Emote[emoteList.size()]);
	}

	private void addEmbedReactions(Message message, Emote... emotes) {
		for(Emote em : emotes) {
			message.addReaction(em).queue();
		}
	}

	private boolean hasEnoughCommandParameters(String[] args) {
		return args.length > 1;
	}

	@Override
	public boolean validate() {
		String guildId = textChannel.getManager().getGuild().getId();
		Document championship = championshipRepository.findOpenChampionship(guildId);
		
		if(championship == null) {
			LOG.severe("Could not find an open Championship for Guild ID: " + guildId + ". Race creation aborted!");
			textChannel.sendMessage(MessageService.getInstance().getMessage("championship.not.created")) 
	 		   .queue(message -> {
	 			   RaceDataSingleton.getInstance().setActiveRaceMessageId(message.getId());
	 		   });
			return false;
		}
		
		LOG.severe("Found an open Championship for Guild ID: " + guildId + ". Race will be created!");
		
		this.championship.setGuildId(championship.getString("guildId"));
		this.championship.setGuildName(championship.getString("guildName"));
		this.championship.setId(championship.getObjectId("_id"));
		
		Date initialDate = championship.getDate("initialDateTime");
		if(initialDate != null){
			this.championship.setInitialDateTime(initialDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
		}
		
		return true;
	}
	
	private Race createRace(String[] args, String raceMessageId) {
		Race race = new Race();
		race.setChampionship(this.championship);
		
		String dateFormat = MessageService.getInstance().getMessage("date.time.format");
		
		//Example: !createRace Bahrain 57 27/03/2021-12:00 28/03/2021-12:00
		if(args[1] != null) {
			CircuitRepository circuitRepository = new CircuitRepository();
			Circuit circuit = circuitRepository.find(args[1]);
			
			if(circuit != null) {
				race.setCircuit(circuit);
			} else {
				textChannel.sendMessage(MessageService.getInstance().getMessage("race.circuit.not.found", args[1])) 
		 		   		   .queue();
				throw new IllegalArgumentException("Couldnt find circuit named " + args[1]);
			}
		}
		
		if(args[2] != null) {
			race.setLaps(Integer.parseInt(args[2]));
		}
		
		if(args[3] != null) {
			race.setQualifyDateTime(LocalDateTime.parse(args[3], DateTimeFormatter.ofPattern(dateFormat)));
		}
		
		if(args[4] != null) {
			race.setRaceDateTime(LocalDateTime.parse(args[4], DateTimeFormatter.ofPattern(dateFormat)));
		}
		
		return race;
	}
}
