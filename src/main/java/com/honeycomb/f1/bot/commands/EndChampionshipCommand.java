package com.honeycomb.f1.bot.commands;

import org.bson.Document;

import com.honeycomb.f1.bot.data.RaceDataSingleton;
import com.honeycomb.f1.bot.repository.ChampionshipRepository;
import com.honeycomb.f1.bot.service.MessageService;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

public class EndChampionshipCommand implements Command{
	
	private Message message;
	private TextChannel textChannel;
	private ChampionshipRepository championshipRepository;
	
	public EndChampionshipCommand(Message message, TextChannel textChannel) {
		this.message = message;
		this.textChannel = textChannel;
		this.championshipRepository = new ChampionshipRepository();
	}
	
	@Override
	public void execute() {
		if(validate()) {
			Document championship = championshipRepository.endChampionship(message.getGuild());
			
			if(championship != null) {
				textChannel.sendMessage(MessageService.getInstance().getMessage("championship.ended", message.getGuild().getName())) 
				.queue(message -> {
					RaceDataSingleton.getInstance().setActiveRaceMessageId(message.getId());
				});
			}
		}
	}
	
	@Override
	public boolean validate() {
		String guildId = message.getGuild().getId();
		Document championship = championshipRepository.findOpenChampionship(guildId);
		
		if(championship == null) {
			textChannel.sendMessage(MessageService.getInstance().getMessage("championship.cannot.end", message.getGuild().getName())) 
		 		   .queue(message -> {
		 			   RaceDataSingleton.getInstance().setActiveRaceMessageId(message.getId());
		 		   });
				return false;
			}
			
			return true;
		}
	}
