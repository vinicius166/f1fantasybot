package com.honeycomb.f1.bot.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.User;

public class BetRepository {
	
	Logger LOG = Logger.getLogger(this.getClass().getName());
	
	public void addBet(User user, Emote driver) {
		Map<String, List<String>> userDriverMap = RaceDataSingleton.getInstance().getUserDriverMap();
		List<String> driverList = null;
		
		if(userDriverMap.containsKey(user.getId())) {
			driverList = userDriverMap.get(user.getId());
			
			LOG.info("Adicionando piloto " + driver.getName() + " ao usu�rio " + user.getName());
			driverList.add(driver.getId());
			
		} else {
			driverList = new ArrayList<String>();
			LOG.info("Criando aposta para o usu�rio " + user.getName() + "com o piloto " + driver.getName());
			driverList.add(driver.getId());
			userDriverMap.put(user.getId(), driverList);
		}
	}
	
}
