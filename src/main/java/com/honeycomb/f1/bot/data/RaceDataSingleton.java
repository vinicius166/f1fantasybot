package com.honeycomb.f1.bot.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class RaceDataSingleton {
	
	Logger LOG = Logger.getLogger(this.getClass().getName());

    private static RaceDataSingleton INSTANCE;
    private String activeRaceMessageId = null;
    private Map<String, List<String>> userDriverMap;
    
    private RaceDataSingleton() {        
    }
    
    public synchronized static RaceDataSingleton getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new RaceDataSingleton();
        }
        
        return INSTANCE;
    }

	public String getActiveRaceMessageId() {
		return activeRaceMessageId;
	}

	public void setActiveRaceMessageId(String activeRaceMessageId) {
		LOG.info("Caching active RACE ID: "+ activeRaceMessageId);
		this.activeRaceMessageId = activeRaceMessageId;
	}

	public Map<String, List<String>> getUserDriverMap() {
		if(this.userDriverMap == null) {
			this.userDriverMap = new HashMap<String, List<String>>();
		}
		
		return userDriverMap;
	}


}
