package com.honeycomb.f1.bot.domain;

public enum Domains {
	
	BOT_NAME("F1-Fantasy"),
	PREFIX("!"),
	CREATE_RACE("createRace"),
	GRAND_PRIX(" GP"),
	COMMAND_SPLIT_PATTERN("\\s+"),
	DATABASE_PROPERTY_FILE("src/main/resources/mongodb.properties"),
	MESSAGES_PROPERTY_FILE("src/main/resources/messages.properties")
	;
	
	public String name;

	Domains(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
