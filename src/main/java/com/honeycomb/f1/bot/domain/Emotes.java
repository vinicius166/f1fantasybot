package com.honeycomb.f1.bot.domain;

import net.dv8tion.jda.api.entities.Emote;

public enum Emotes {
	HAMILTON("F1_Hamilton"),
	BOTTAS("F1_Bottas"),
	VERSTAPPEN("F1_Verstappen"), 
	PEREZ("F1_Perez"),
	NORRIS("F1_Norris"),
	RICCIARDO("F1_Ricciardo"), 
	VETTEL("F1_Vettel"), 
	STROLL("F1_Stroll"), 
	ALONSO("F1_Alonso"),
	OCON("F1_Ocon"),
	LECLERC("F1_Leclerc"),
	SAINZ("F1_Sainz"),
	GASLY("F1_Gasly"),
	TSUNODA("F1_Tsunoda"), 
	RAIKKONEN("F1_Raikkonen"), 
	GIOVINAZZI("F1_Giovinazzi"),
	SCHUMACHER("F1_Schumacher"), 
	MAZEPIN("F1_Mazepin"),
	RUSSELL("F1_Russell"), 
	LATIFI("F1_Latifi"),
	;
	
	public String name;

	Emotes(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isF1Emote(Emote emote) {
		for(Emotes em : Emotes.values()) {
			if(em.getName().equalsIgnoreCase(emote.getName())) {
				return true;
			}
		}
		return false;
	}
	
}
