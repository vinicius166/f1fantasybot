package com.honeycomb.f1.bot.main;

import javax.security.auth.login.LoginException;

import com.honeycomb.f1.bot.listeners.GuildMessageReactionAdd;
import com.honeycomb.f1.bot.listeners.GuildMessageReceived;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;

public class F1Fantasy {
	
	private static JDA discordService;

	public static void main(String[] args) throws LoginException {
		discordService = JDABuilder.createDefault(System.getenv("botToken")).build();
		discordService.getPresence().setActivity(Activity.playing("s🅱inalla 🏎🏎🏎"));
		discordService.addEventListener(new GuildMessageReceived());
		discordService.addEventListener(new GuildMessageReactionAdd());
	}
}
