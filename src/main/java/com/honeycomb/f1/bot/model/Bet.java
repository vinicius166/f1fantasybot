package com.honeycomb.f1.bot.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import net.dv8tion.jda.api.entities.User;

public class Bet extends BaseModel{
	private User user;
	private LinkedList<Driver> driverList;
	
	private String userId;
	private String championshipId;
	private String raceId;
	private List<Driver> drivers;
	

	public LinkedList<Driver> getDriverList() {
		if(this.driverList == null) {
			this.driverList = new LinkedList<Driver>();
		}
		
		return driverList;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getChampionshipId() {
		return championshipId;
	}

	public void setChampionshipId(String championshipId) {
		this.championshipId = championshipId;
	}

	public String getRaceId() {
		return raceId;
	}

	public void setRaceId(String raceId) {
		this.raceId = raceId;
	}

	public List<Driver> getDrivers() {
		if(this.drivers == null) {
			this.drivers = new ArrayList<Driver>();
		}
		return drivers;
	}

	public void setDrivers(List<Driver> drivers) {
		this.drivers = drivers;
	}

	public void setDriverList(LinkedList<Driver> driverList) {
		this.driverList = driverList;
	}

}
