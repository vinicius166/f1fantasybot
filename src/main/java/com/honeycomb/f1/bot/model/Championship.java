package com.honeycomb.f1.bot.model;

import java.time.LocalDateTime;

public class Championship extends BaseModel{

	private String guildId;
	
	private String guildName;
	
	private LocalDateTime initialDateTime;
	
	private LocalDateTime endDateTime;

	public String getGuildId() {
		return guildId;
	}

	public void setGuildId(String guildId) {
		this.guildId = guildId;
	}

	public String getGuildName() {
		return guildName;
	}

	public void setGuildName(String guildName) {
		this.guildName = guildName;
	}

	public LocalDateTime getInitialDateTime() {
		return initialDateTime;
	}

	public void setInitialDateTime(LocalDateTime initialDateTime) {
		this.initialDateTime = initialDateTime;
	}

	public LocalDateTime getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(LocalDateTime endDateTime) {
		this.endDateTime = endDateTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDateTime == null) ? 0 : endDateTime.hashCode());
		result = prime * result + ((guildId == null) ? 0 : guildId.hashCode());
		result = prime * result + ((guildName == null) ? 0 : guildName.hashCode());
		result = prime * result + ((initialDateTime == null) ? 0 : initialDateTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Championship other = (Championship) obj;
		if (endDateTime == null) {
			if (other.endDateTime != null)
				return false;
		} else if (!endDateTime.equals(other.endDateTime))
			return false;
		if (guildId == null) {
			if (other.guildId != null)
				return false;
		} else if (!guildId.equals(other.guildId))
			return false;
		if (guildName == null) {
			if (other.guildName != null)
				return false;
		} else if (!guildName.equals(other.guildName))
			return false;
		if (initialDateTime == null) {
			if (other.initialDateTime != null)
				return false;
		} else if (!initialDateTime.equals(other.initialDateTime))
			return false;
		return true;
	}
	
}
