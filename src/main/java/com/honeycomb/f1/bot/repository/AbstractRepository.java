package com.honeycomb.f1.bot.repository;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.honeycomb.f1.bot.model.BaseModel;
import com.mongodb.BasicDBObject;
import com.mongodb.client.model.FindOneAndReplaceOptions;

public class AbstractRepository<T extends BaseModel>{
	
	private Class<T> type;
	
	protected AbstractRepository(Class<T> type) {
        this.type = type;
    }

	public Document findFirst(BasicDBObject query) {
		return MongoDBClient.getInstance().findFirst(this.type.getSimpleName(), query);
	}
	
	public ArrayList<Document> findAll(BasicDBObject query) {
		return MongoDBClient.getInstance().findAll(this.type.getSimpleName(), query);
	}
	
	@SuppressWarnings("unchecked")
	public List<T> findAll(List<? extends Bson> aggregateList) {
		return (List<T>) MongoDBClient.getInstance().findAll(this.type.getSimpleName(), aggregateList);
	}
	
	@SuppressWarnings("unchecked")
	public T find(List<? extends Bson> aggregateList) {
		return (T) MongoDBClient.getInstance().find(this.type, aggregateList);
	}
	
	public Document save(Document document) {
		return MongoDBClient.getInstance().save(this.type.getSimpleName(), document);
	}
	
	public T save(T model) {
		return MongoDBClient.getInstance().save(this.type.getSimpleName(), model);
	}
	
	public Document findAndUpdate(BasicDBObject query, BasicDBObject changes) {
		return MongoDBClient.getInstance().findAndUpdate(this.type.getSimpleName(), query, changes);
	}

	public T findOneAndReplace(Document filterById, FindOneAndReplaceOptions options, T model) {
		return MongoDBClient.getInstance().findAndUpdate(filterById, options, model);
	}
}
