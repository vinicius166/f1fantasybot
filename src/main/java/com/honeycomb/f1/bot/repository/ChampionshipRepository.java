package com.honeycomb.f1.bot.repository;

import java.time.LocalDateTime;

import org.bson.Document;

import com.honeycomb.f1.bot.model.Championship;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;

import net.dv8tion.jda.api.entities.Guild;

public class ChampionshipRepository extends AbstractRepository<Championship>{

	public ChampionshipRepository() {
		super(Championship.class);
	}

	public Document findOpenChampionship(String guildId) {
		BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder.start()
																  .append("guildId", guildId)
																  .append("initialDateTime", new BasicDBObject("$exists", true))
																  .append("endDateTime", new BasicDBObject("$eq", null))
																  .get();
		return findFirst(query);
	}
	
	public Document createChampionship(Guild guild) {
		Document championship = new Document("guildId", guild.getId())
	               .append("guildName", guild.getName())
	               .append("initialDateTime", LocalDateTime.now())
	               .append("endDateTime", null);
		
		return save(championship);
	}
	
	public Document endChampionship(Guild guild) {
		BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder.start()
				  .append("guildId", guild.getId())
				  .append("initialDateTime", new BasicDBObject("$exists", true))
				  .append("endDateTime", new BasicDBObject("$eq", null))
				  .get();
		
		BasicDBObject changes = (BasicDBObject) BasicDBObjectBuilder.start()
													.append("$set", new BasicDBObject("endDateTime", LocalDateTime.now())).get();
		
		return findAndUpdate(query, changes);
	}
}
