package com.honeycomb.f1.bot.repository;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.eq;

import java.util.Arrays;
import java.util.logging.Logger;

import org.bson.conversions.Bson;

import com.honeycomb.f1.bot.model.Circuit;

public class CircuitRepository extends AbstractRepository<Circuit>{
	
	Logger LOG = Logger.getLogger(this.getClass().getName());

	public CircuitRepository() {
		super(Circuit.class);
	}
	
	public Circuit find(String circuitName){
		Bson match = match(eq("name", circuitName));
		return super.find(Arrays.asList(match));
	}

}
