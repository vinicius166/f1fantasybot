package com.honeycomb.f1.bot.repository;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.eq;

import java.util.Arrays;
import java.util.logging.Logger;

import org.bson.conversions.Bson;

import com.honeycomb.f1.bot.model.Driver;

public class DriverRepository extends AbstractRepository<Driver>{
	
	Logger LOG = Logger.getLogger(this.getClass().getName());

	public DriverRepository() {
		super(Driver.class);
	}
	
	public Driver find(String emoteId) {
		Bson driverMatch = match(eq("emoteId", emoteId));
		return find(Arrays.asList(driverMatch));
	}

}
