package com.honeycomb.f1.bot.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Properties;

import com.honeycomb.f1.bot.domain.Domains;

public class MessageService {
	private Properties properties;
	
	private static MessageService INSTANCE;
    
    private MessageService() {
    	
    	try (InputStream input = new FileInputStream(Domains.MESSAGES_PROPERTY_FILE.getName())) {
    		properties = new Properties();
    		properties.load(input);
    		
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public synchronized static MessageService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new MessageService();
        }
        
        return INSTANCE;
    }
    
    public String getMessage(String key, Object...parameters) {
    	return MessageFormat.format(properties.getProperty(key), parameters);
    }
    
    public String getMessage(String key) {
    	return properties.getProperty(key);
    }
}
